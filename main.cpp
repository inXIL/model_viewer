#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

#include <GL/freeglut.h>

#include <vecmath.h>


using namespace std;

// You will need more global variables to implement color and position changes

namespace color {
using Color4f = GLfloat[4];
namespace {
Color4f colors[] = {
    {0.5, 0.5, 0.9, 1.0},
    {0.9, 0.5, 0.5, 1.0},
    {0.5, 0.9, 0.3, 1.0},
    {0.3, 0.8, 0.9, 1.0}
};
constexpr size_t colors_len = sizeof(colors) / sizeof(colors[0]);
size_t color_index = 0;
}
void increment_color_index() {
    ++color_index;
    color_index %= colors_len;
}
GLfloat* get_current_color() { // types are for the weak
    return colors[color_index];
}
} // namespace color

namespace light_position {
namespace {
GLfloat light_x = 1.0f;
GLfloat light_y = 1.0f;
}
void increment_light_x() {
    light_x += 0.5f;
}
void decrement_light_x() {
    light_x -= 0.5f;
}
void increment_light_y() {
    light_y += 0.5f;
}
void decrement_light_y() {
    light_y -= 0.5f;
}
GLfloat get_light_x() {
    return light_x;
}
GLfloat get_light_y() {
    return light_y;
}
} // namespace light_position

// Q: why would i write obj parser for a third time?
// A: no idea, but i did bare minimum of it
namespace obj {
namespace {
vector<Vector3f> vertex_coords;
vector<Vector3f> vertex_normals;
vector<size_t> vertex_coords_indices;
vector<size_t> vertex_normals_indices;
}
void read_obj_from_cin() {
    for(string line; getline(cin, line); ) {
        stringstream line_stream(line);
        string line_desc;
        line_stream >> line_desc;
        if ("v" == line_desc) {
            GLfloat x, y, z;
            line_stream >> x >> y >> z;
            vertex_coords.emplace_back(x, y, z);
        }
        else if ("vn" == line_desc) {
            GLfloat nx, ny, nz;
            line_stream >> nx >> ny >> nz;
            vertex_normals.emplace_back(nx, ny, nz);
        }
        else if ("mtl" == line_desc) {
            // unused
        }
        else if ("g" == line_desc) {
            // dont care
        }
        else if ("f" == line_desc) {
            // just ignore everything, including textures
            for(int i = 0; i < 3; ++i) { // gimme range for
                string s; line_stream >> s;
                const auto cnt_slash = count(s.begin(), s.end(), '/');
                stringstream single_vertex_indices(s);
                size_t coord_index, _texture_index, normal_index;
                switch(cnt_slash) {
                    case 0: {
                        single_vertex_indices >> coord_index;
                        vertex_coords_indices.emplace_back(coord_index - 1);
                    } break;
                    case 1: {
                        single_vertex_indices >> coord_index >> _texture_index; // discard texture index
                        vertex_coords_indices.emplace_back(coord_index - 1);
                    } break;
                    case 2: {
                        single_vertex_indices >> coord_index;
                        string s1, s2;
                        getline(single_vertex_indices, s1, '/'); // discard empty string
                        getline(single_vertex_indices, s2, '/'); // discard texture string, even if empty
                        single_vertex_indices >> normal_index;
                        vertex_coords_indices.emplace_back(coord_index - 1);
                        vertex_normals_indices.emplace_back(normal_index - 1);
                    } break;
                }
            }
        }
        else {
            // dont care
        }
    }
}
void outdated_draw_with_gl() {
    // this is so-o-o-o outdated:
    glBegin(GL_TRIANGLES);
    for(int i = 0; i < (int)vertex_coords_indices.size(); ++i) {
        const auto& n = vertex_normals[vertex_normals_indices[i]];
        const auto& pos = vertex_coords[vertex_coords_indices[i]];
        glNormal3d(n.x(), n.y(), n.z());
        glVertex3d(pos.x(), pos.y(), pos.z());
    }
    glEnd();
}
} // namespace obj

namespace camera {
namespace {
Vector3f start_pos{ 0.0f, 0.0f, 5.0f };
Vector3f start_dir{ 0.0f, 0.0f, -1.0f };
}
namespace {
Vector3f pos{ start_pos }; // position
Vector3f dir{ start_dir }; // direction, normalized
}
void reset() {
    pos = start_pos;
    dir = start_dir;
}
float get_dist() {
    return sqrt(Vector3f::dot(pos, pos));
}
const Vector3f& get_camera_pos() {
    return pos;
}
namespace {
constexpr int MAX_PERIOD_MILLISEC = 1000/60;
}
namespace {
constexpr float ZOOM_COEFF = 0.02f;
} // i feel like without timers it was better
void zoom_in() {
    glutTimerFunc(
        MAX_PERIOD_MILLISEC,
        [](int){
            pos += ZOOM_COEFF * get_dist() * dir;
        },
        0
    );
}
void zoom_out() {
    glutTimerFunc(
        MAX_PERIOD_MILLISEC,
        [](int){
            pos -= ZOOM_COEFF * get_dist() * dir;
        },
        0
    );
}
namespace {
constexpr float Y_SPIN = 0.11;
}
namespace {
bool is_spinning = true;
Matrix4f modelview = Matrix4f::identity();
}
void toggle_spin() {
    is_spinning = !is_spinning;
}
void spin_if_toggled() {
    glutTimerFunc(
        MAX_PERIOD_MILLISEC,
        [](int is_spinning){
            if (is_spinning) {
                modelview = Matrix4f::rotateY(Y_SPIN) * modelview;
            }
        },
        is_spinning
    );
}
Matrix4f get_spin_rotmat() {
    return modelview;
}
Matrix4f get_modelview() { // whatever
    return Matrix4f::lookAt(pos, {0, 0, 0}, {0, 1, 0}) * modelview;
}
} // namespace camera


// with all those halfbaked math libs and freeglut
// it feels like godd**n handmade hero
namespace mouse {
namespace {
bool left_down = false;
Vector2f start;
Vector2f finish;
Vector3f start_unproj;
Vector3f finish_unproj;
Matrix4f pan = Matrix4f::identity();
}
// mostly shamelessly stolen from wikibooks.org
void onMouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT && state == GLUT_DOWN) {
        left_down = true;
        start = {x, y};
    }
    else {
        left_down = false;
    }
}
void onMotion(int x, int y) { // motion with mouse held
    if (left_down) {
        finish = {x, y};
    }
}
// actually wanted to unproject, then intersect with camera sphere
// but with those libs i'm not gonna bother
// i had my doze of unprojects from dx9 already
// ideally should be using quaternions and object to find path on shouldn't probably be a sphere
Vector3f sphere_project(Vector2f vec) {
    auto proj = Vector3f{
        1.0 * vec.x() / 800 * 2 - 1.0,
        -(1.0 * vec.y() / 800 * 2 - 1.0),
        0
    };
    const auto dot = Vector3f::dot(proj, proj);
    if (dot <= 1*1) {
        proj.z() = sqrt(1*1 - dot);
    }
    else {
        proj.normalize();
    }
    return proj;
}
void mult_camera_pan() {
    if (left_down && abs(start.x() - finish.x()) > 1e-5 && abs(start.y() - finish.y()) > 1e-5) {
        const auto p1 = sphere_project(start);
        const auto p2 = sphere_project(finish);
        float angle = acos(min(1.0f, Vector3f::dot(p1, p2)));
        const auto axis = camera::get_modelview().inverse() *  Vector4f{Vector3f::cross(p1, p2), 0};
        pan = Matrix4f::rotation(axis.xyz(), angle) * pan;
        start = finish;
    }
}
Matrix4f get_camera_pan() {
    return pan;
}
}

// These are convenience functions which allow us to call OpenGL
// methods on Vec3d objects
inline void glVertex(const Vector3f &a)
{ glVertex3fv(a); }

inline void glNormal(const Vector3f &a)
{ glNormal3fv(a); }


// This function is called whenever a "Normal" key press is received.
void keyboardFunc( unsigned char key, int x, int y )
{
    switch ( key )
    {
    case 27: // Escape key
        exit(0);
        break;
    case 'c': {
        color::increment_color_index();
    } break;
    case 'r': {
        camera::toggle_spin();
    } break;
    case 'z': {
        camera::zoom_in();
    } break;
    case 'x': {
        camera::zoom_out();
    } break;
    case 't': {
        camera::reset();
    } break;
    default:
        cout << "Unhandled key press " << key << "." << endl;
    }

	// this will refresh the screen so that the user sees the color change
    glutPostRedisplay();
}

// This function is called whenever a "Special" key press is received.
// Right now, it's handling the arrow keys.
void specialFunc( int key, int x, int y )
{
    switch ( key )
    {
    case GLUT_KEY_UP: { // the order of Y inc/dec is not apparent though, would be natural to inc here
        light_position::increment_light_y();
    } break;
    case GLUT_KEY_DOWN: {
        light_position::decrement_light_y();
    } break;
    case GLUT_KEY_LEFT: {
        light_position::decrement_light_x();
    } break;
    case GLUT_KEY_RIGHT: {
        light_position::increment_light_x();
    } break;
    }

	// this will refresh the screen so that the user sees the light position
    glutPostRedisplay();
}

// This function is responsible for displaying the object.
void drawScene(void)
{
    int i;

    // Clear the rendering window
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Set material properties of object

	// Here are some colors you might use - feel free to add more
    // Rotate the image
    glMatrixMode( GL_MODELVIEW );  // Current matrix affects objects positions
    glLoadMatrixf(camera::get_modelview() * mouse::get_camera_pan());

	// Here we use the first color entry as the diffuse color
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color::get_current_color());

	// Define specular color and shininess
    GLfloat specColor[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat shininess[] = {100.0};

	// Note that the specular color and shininess can stay constant
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specColor);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shininess);

    // Set light properties

    // Light color (RGBA)
    GLfloat Lt0diff[] = {1.0,1.0,1.0,1.0};
    // Light position
	GLfloat Lt0pos[] = {
        light_position::get_light_x(),
        light_position::get_light_y(),
        5.0f, 1.0f
    };

    glLightfv(GL_LIGHT0, GL_DIFFUSE, Lt0diff);
    glLightfv(GL_LIGHT0, GL_POSITION, Lt0pos);

	// This GLUT method draws a teapot.  You should replace
	// it with code which draws the object you loaded.
    //glutSolidTeapot(1.0);

    obj::outdated_draw_with_gl();

    // Dump the image to the screen.
    glutSwapBuffers();
}

// Initialize OpenGL's rendering modes
void initRendering()
{
    glEnable(GL_DEPTH_TEST);   // Depth testing must be turned on
    glEnable(GL_LIGHTING);     // Enable lighting calculations
    glEnable(GL_LIGHT0);       // Turn on light #0.
}

// Called when the window is resized
// w, h - width and height of the window in pixels.
void reshapeFunc(int w, int h)
{
    // Always use the largest square viewport possible
    if (w > h) {
        glViewport((w - h) / 2, 0, h, h);
    } else {
        glViewport(0, (h - w) / 2, w, w);
    }

    // Set up a perspective view, with square aspect ratio
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // 50 degree fov, uniform aspect ratio, near = 1, far = 100
    gluPerspective(50.0, 1.0, 1.0, 100.0);
}

void loadInput()
{
	// load the OBJ file here
}

// Main routine.
// Set up OpenGL, define the callbacks and start the main loop
int main( int argc, char** argv )
{
    obj::read_obj_from_cin();
    loadInput();

    glutInit(&argc,argv);

    // We're going to animate it, so double buffer
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );

    // Initial parameters for window position and size
    glutInitWindowPosition( 60, 60 );
    glutInitWindowSize( 800, 800 );
    glutCreateWindow("Assignment 0");

    // Initialize OpenGL parameters.
    initRendering();

    // Set up callback functions for key presses
    glutKeyboardFunc(keyboardFunc); // Handles "normal" ascii symbols
    glutSpecialFunc(specialFunc);   // Handles "special" keyboard keys

    glutMouseFunc(mouse::onMouse);
    glutMotionFunc(mouse::onMotion);

     // Set up the callback function for resizing windows
    glutReshapeFunc( reshapeFunc );

    glutIdleFunc([](){
        camera::spin_if_toggled();
        mouse::mult_camera_pan();
        glutPostRedisplay();
    });

    // Call this whenever window needs redrawing
    glutDisplayFunc( drawScene );

    // Start the main loop.  glutMainLoop never returns.
    glutMainLoop( );

    return 0;	// This line is never reached.
}
