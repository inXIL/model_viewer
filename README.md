# Model Viewer
### how to use:
```shell
make
./a0 < garg.obj
```
### hotkeys:
* z/x -- zoom in/out
* c -- change color
* r -- toggle spin
* t -- reset zoom
* LeftMouse + drag -- pan
### issues:
* mesh simplification is not implemented, no time
* no CMAKE, pure Makefile
* freeglut doesn't seem to provide glGenBuffers/glBindBuffers? what should i do in order to get buffered indexed draw?
* those libs are way too outdated, should be using something along the lines of glfw, sdl, glad, glm, eigen, etc...
* color change is not smooth, but i don't want to make it smooth though
* math is probably busted
* panning is having seizures of gimbal locks or possibly plain broken code
* should be using quaternions instead of (axis, angle) pair
* should be using a different smooth surface for panning
* code quality is junk