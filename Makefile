INCFLAGS  = -I include/GL
INCFLAGS += -I include/vecmath

LINKFLAGS  = -lglut -lGL -lGLU
LINKFLAGS += -L lib

CFLAGS    = -O2
CC        = g++
SRCS      = vecmath/Vector2f.cpp vecmath/Vector3f.cpp vecmath/Matrix4f.cpp vecmath/Matrix3f.cpp vecmath/Quat4f.cpp vecmath/Matrix2f.cpp vecmath/Vector4f.cpp main.cpp
OBJS      = $(SRCS:.cpp=.o)
PROG      = a0

all: $(SRCS) $(PROG)

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@ $(INCFLAGS) $(LINKFLAGS)

.cpp.o:
	$(CC) $(CFLAGS) $< -c -o $@ $(INCFLAGS)

depend:
	makedepend $(INCFLAGS) -Y $(SRCS)

clean:
	rm $(OBJS) $(PROG)

